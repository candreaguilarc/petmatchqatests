$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("iOSLoginOnPetMatch.feature");
formatter.feature({
  "line": 4,
  "name": "Scenarios of Login with iOS",
  "description": "",
  "id": "scenarios-of-login-with-ios",
  "keyword": "Feature",
  "tags": [
    {
      "line": 2,
      "name": "@pettinder"
    },
    {
      "line": 2,
      "name": "@testIos"
    }
  ]
});
formatter.scenarioOutline({
  "line": 6,
  "name": "Sign In into PetMatch with iOS",
  "description": "",
  "id": "scenarios-of-login-with-ios;sign-in-into-petmatch-with-ios",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 8,
  "name": "Ingresar a la pagina Login PetMatch",
  "keyword": "Given "
});
formatter.step({
  "line": 9,
  "name": "Ingresar el email \"\u003cemail\u003e\"",
  "keyword": "When "
});
formatter.step({
  "line": 10,
  "name": "Ingresar la contraseña \"\u003cpassword\u003e\"",
  "keyword": "And "
});
formatter.step({
  "line": 11,
  "name": "Click en el boton Login",
  "keyword": "And "
});
formatter.step({
  "line": 12,
  "name": "Se mostrar el HomePage",
  "keyword": "Then "
});
formatter.examples({
  "line": 14,
  "name": "",
  "description": "",
  "id": "scenarios-of-login-with-ios;sign-in-into-petmatch-with-ios;",
  "rows": [
    {
      "cells": [
        "email",
        "password"
      ],
      "line": 15,
      "id": "scenarios-of-login-with-ios;sign-in-into-petmatch-with-ios;;1"
    },
    {
      "cells": [
        "maver@hotmail.com",
        "Admin123"
      ],
      "line": 16,
      "id": "scenarios-of-login-with-ios;sign-in-into-petmatch-with-ios;;2"
    }
  ],
  "keyword": "Examples"
});
formatter.scenario({
  "line": 16,
  "name": "Sign In into PetMatch with iOS",
  "description": "",
  "id": "scenarios-of-login-with-ios;sign-in-into-petmatch-with-ios;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 2,
      "name": "@pettinder"
    },
    {
      "line": 2,
      "name": "@testIos"
    }
  ]
});
formatter.step({
  "line": 8,
  "name": "Ingresar a la pagina Login PetMatch",
  "keyword": "Given "
});
formatter.step({
  "line": 9,
  "name": "Ingresar el email \"maver@hotmail.com\"",
  "matchedColumns": [
    0
  ],
  "keyword": "When "
});
formatter.step({
  "line": 10,
  "name": "Ingresar la contraseña \"Admin123\"",
  "matchedColumns": [
    1
  ],
  "keyword": "And "
});
formatter.step({
  "line": 11,
  "name": "Click en el boton Login",
  "keyword": "And "
});
formatter.step({
  "line": 12,
  "name": "Se mostrar el HomePage",
  "keyword": "Then "
});
formatter.match({
  "location": "iOSPetMatchLoginSteps.ingresarALaPaginaLoginPetMatch()"
});
formatter.result({
  "duration": 17456473342,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "maver@hotmail.com",
      "offset": 19
    }
  ],
  "location": "iOSPetMatchLoginSteps.ingresarElEmail(String)"
});
formatter.result({
  "duration": 4561794349,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Admin123",
      "offset": 24
    }
  ],
  "location": "iOSPetMatchLoginSteps.ingresarLaContraseña(String)"
});
formatter.result({
  "duration": 5676812865,
  "status": "passed"
});
formatter.match({
  "location": "iOSPetMatchLoginSteps.clickEnElBotonLogin()"
});
formatter.result({
  "duration": 1167329401,
  "status": "passed"
});
formatter.match({
  "location": "iOSPetMatchLoginSteps.seMostrarElHomePage()"
});
formatter.result({
  "duration": 6819426120,
  "status": "passed"
});
});