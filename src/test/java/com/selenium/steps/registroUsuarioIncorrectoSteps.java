package com.selenium.steps;

import com.selenium.framework.ParentScenario;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.testng.Assert;

public class registroUsuarioIncorrectoSteps extends ParentScenario {

    @Given("^I open the app$")
    public void iOpenTheApp() {
        System.out.println("Abre la aplicacion");
        startIOS();
    }

    @When("^Click Register Now button$")
    public void clickRegisterNowButton() {
        registroUsuarioPage.clickOnButtonRegisterNow();
        System.out.println("Hace click en Register now");
    }

    @And("^Type the user name \"([^\"]*)\"$")
    public void typeTheUserName(String name)  {
        registroUsuarioPage.inputName(name);
        System.out.println("El usuario escribe su nombre: Carlo");
    }

    @And("^Type the email \"([^\"]*)\"$")
    public void typeTheEmail(String email) {
        registroUsuarioPage.inputEmail(email);
        System.out.println("El usuario escribe su email: carlo@hotmail.com");
    }

    @And("^Type the password \"([^\"]*)\"$")
    public void typeThePassword(String password) {
        registroUsuarioPage.inputPassword(password);
        System.out.println("El usuario escribe una contraseña de 6 digitos");
    }

    @And("^Re-Type the password \"([^\"]*)\"$")
    public void reTypeThePassword(String passwordConfirmation)  {
        registroUsuarioPage.inputPasswordConfirmation(passwordConfirmation);
        System.out.println("El usuario escribe una contraseña incorrecta");

    }

    @And("^Click Register button$")
    public void clickRegisterButton() {
        registroUsuarioPage.clickOnButtonRegister();
        System.out.println("Hace click en Registrar");
    }

    @Then("^An alert is shown$")
    public void anAlertIsShown() {
        System.out.println("Se presenta una alerta");
        Assert.assertTrue(registroUsuarioPage.RecyclerAlertDisplayed());

    }
}
