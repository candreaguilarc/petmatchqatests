package com.selenium.steps;
import com.selenium.framework.ParentScenario;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.testng.Assert;

public class editarMascotaSteps extends ParentScenario{

    @Given("^I'm into Home section$")
    public void iMIntoHomeSection() {
        System.out.println("Usuario esta en Home");
        startIOS();
    }

    @When("^Click Pets Button$")
    public void clickPetsButton() {
        editarMascotaPage.clickOnButtonPets();
        System.out.println("Usuario ingresa a la seccion Pets");
    }

    @And("^CLick the Edit button$")
    public void clickTheEditButton() {
        editarMascotaPage.clickOnButtonEdit();
        System.out.println("Usuario hace click en Edit");
    }

    @And("^Type the pet name \"([^\"]*)\"$")
    public void typeThePetName(String name) {
        editarMascotaPage.inputName(name);
        System.out.println("Usuario ingresa nombre");
    }

    @And("^Select the month\"([^\"]*)\"$")
    public void selectTheMonth(String month) {
        editarMascotaPage.setMonth(month);
        System.out.println("Usuario selecciona mes");
    }

    @And("^Select the day\"([^\"]*)\"$")
    public void selectTheDay(String day) {
        editarMascotaPage.setDay(day);
        System.out.println("Usuario selecciona dia");
    }

    @And("^Select the year\"([^\"]*)\"$")
    public void selectTheYear(String year){
        editarMascotaPage.setYear(year);
        System.out.println("Usuario selecciona año");
    }

    @And("^Select the genre\"([^\"]*)\"$")
    public void selectTheGenre(String genre){
        editarMascotaPage.setGenre(genre);
        System.out.println("Usuario selecciona genero");
    }

    @And("^Select the type\"([^\"]*)\"$")
    public void selectTheType(String type){
        editarMascotaPage.setType(type);
        System.out.println("Usuario selecciona tipo");
    }

    @And("^Type a description\"([^\"]*)\"$")
    public void typeADescription(String description) {
        editarMascotaPage.inputDescription(description);
        System.out.println("Usuario ingresa descripcion");
    }

    @And("^Click Save Button$")
    public void clickSaveButton() {
        editarMascotaPage.clickOnButtonSave();
        System.out.println("Usuario hace click en Save");
    }


    @Then("^The year has been edited\"([^\"]*)\", \"([^\"]*)\"$")
    public void theYearHasBeenEdited(String oldyear, String year)  {
       System.out.println("El año fue editado");
        Assert.assertEquals(oldyear, year);
    }
}
