package com.selenium.page;

import com.selenium.framework.ParentPage;
import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.By;

public class EliminarMascotaPage extends ParentPage {

    public EliminarMascotaPage(AppiumDriver driver){
        super(driver);
    }

    By BTN_PETS = By.id("Pets");
    By BTN_DELETE = By.xpath("(//XCUIElementTypeButton[@name=\"Delete\"])[1]");
    By PET = By.xpath("//XCUIElementTypeStaticText[@name=\"Cookie\"]");

    public void clickOnButtonPets(){
        handlingWaitToElement(BTN_PETS);
        ClickOnLocator(BTN_PETS);
    }

    public void selectPet(){
        SwipeCell();
    }
    public void clickOnButtonDelete(){
        ClickOnLocator(BTN_DELETE);
    }

    public boolean deletePetList(){
        handlingWaitToElement(PET);
        return locatorDisplayed(PET);
    }
}
