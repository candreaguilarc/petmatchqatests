package com.selenium.framework;

import com.selenium.page.*;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.URL;

public class ParentScenario {
    private AppiumDriver driver;
    private WebDriver driver2;


    protected static String Platform;
    //Android / IOs Pages
    protected static PetmatchLoginPage petmatchLoginPage;
    protected static AgregarMascotaPage agregarMascotaPage;
    protected static RegistroUsuarioPage registroUsuarioPage;
    protected static EditarMascotaPage editarMascotaPage;
    protected static EliminarMascotaPage deletePet;



    public void startIOS(){
        String url = "http://localhost:4723/wd/hub";
        DesiredCapabilities cap = new DesiredCapabilities();
        cap.setCapability(MobileCapabilityType.AUTOMATION_NAME, "XCUITest");
        cap.setCapability(MobileCapabilityType.PLATFORM_NAME, "iOS");
        cap.setCapability(MobileCapabilityType.PLATFORM_VERSION, "12.1");
        cap.setCapability(MobileCapabilityType.DEVICE_NAME,"iPhone XR");
        cap.setCapability(MobileCapabilityType.UDID,"22EBD7C1-DA9F-4A42-887E-0160E41057FA");
        cap.setCapability(MobileCapabilityType.APP, "/Users/fharo/Desktop/lol/PetMatch.app");
        try{
            driver = new IOSDriver(new URL(url),cap);
        }catch (Exception e){
            System.out.println("Excepcion al momento de generar el Driver " + e);
        }
        petmatchLoginPage = new PetmatchLoginPage(driver);
        agregarMascotaPage = new AgregarMascotaPage(driver);
        registroUsuarioPage = new RegistroUsuarioPage(driver);
        editarMascotaPage = new EditarMascotaPage(driver);
        deletePet = new EliminarMascotaPage(driver);
    }
/*
    protected void nativateTo (String url){
        driver2.navigate().to(url);
    }

    protected void closeWebDriver(){
        driver2.quit();
    }

    protected void closeMobileDriver(){
        driver.quit();
    } */

}
