@pettinder @deletePet

Feature: Scenario Delete Pet with iOS

  Scenario Outline: Delete a pet

    Given I am into Home
    When Click pets button
    And Select the Pet "<cell>"
    And Click the Delete button
    Then The pet is not in the list

    Examples:
      | cell   |
      | test   |