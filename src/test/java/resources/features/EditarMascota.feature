
@pettinder @editPet

Feature: Scenario Edit Pet

  Scenario Outline: Edit Pet

    Given I'm into Home section
    When Click Pets Button
    And CLick the Edit button
    And Type the pet name "<name>"
    And Select the month"<month>"
    And Select the day"<day>"
    And Select the year"<year>"
    And Select the genre"<genre>"
    And Select the type"<type>"
    And Type a description"<description>"
    And Click Save Button
    Then The year has been edited"<oldyear>", "<year>"

    Examples:
      | name      |   month      |  day    |  year  |  genre   |    type  |       description           | oldyear  |
      | Cookie    |   September  |   18    |  2018  |  Male    |    Dog   |  Le encantan las galletas   |   2016   |